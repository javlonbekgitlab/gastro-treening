const start = () => {
    let newElement = document.createElement('div')
    let carouselWheelTotal = document.getElementsByClassName('carouselWheelTotal')
    let current = document.getElementsByClassName('carouselWheelCurrent')
    let sliderItems = document.getElementsByClassName('aboutUsContentWaiterCarouselItem')
    const slider = document.querySelector('.aboutUsContentWaiterCarousel')
    const waiterInfoItems = document.getElementsByClassName('aboutUsContentWaiterInfoItem')
    const nav = document.getElementsByClassName('carouselArrow')
    const progress = document.getElementsByClassName('progress')
    const carouselWidth = sliderItems.length * sliderItems[0].clientWidth - sliderItems[0].clientWidth
    let percent = 100 / sliderItems.length
    let percentSum = percent
    let isDown = false
    let startX
    let scrollLeft
    let scroll 
    let scrollSum = 0 
    
    waiterInfoItems[0].style.display = 'block'
    document.getElementsByClassName('aboutUsContentWaiterInfo')[0].style.width = waiterInfoItems[0].clientWidth + 'px'
    document.getElementsByClassName('aboutUsContentWaiterInfo')[0].style.height = waiterInfoItems[0].clientHeight + document.getElementsByClassName('aboutUsContentWaiterCarouselWheelCont')[0].clientHeight + 'px'
    carouselWheelTotal[0].innerHTML = sliderItems.length
    carouselWheelTotal[1].innerHTML = sliderItems.length
    progress[0].style.width = `${percentSum}%`
    progress[1].style.width = `${percentSum}%`

    newElement.innerHTML = '<div class = "box"></div>'
    slider.appendChild(newElement)
    document.getElementsByClassName('box')[0].style.width = `${slider.clientWidth - sliderItems[0].clientWidth}px`
    sliderItems[parseInt(current[0].innerHTML) - 1].classList.add('aboutUsContentWaiterCarouselItemActive')

    for (let i = 0; i < nav.length; i++) {
        nav[i].addEventListener('click', () => scrollCarousel(i))
    }

    for (let i = 0; i < sliderItems.length; i++) sliderItems[i].addEventListener('click', () => scrollClick(i))

    // const progressFunc = walk => {
    //     if (scrollLeft > carouselWidth) {
    //         current.innerHTML = sliderItems.length
    //         progress[0].style.width = `${current.innerHTML * percent}%`
    //     }
    //     else {
    //         scrollSum = ((scrollLeft - walk >= 0) && (scrollLeft - walk <= carouselWidth)) ? scrollLeft - walk : scrollSum 
    //         current.innerHTML = Math.ceil((scrollSum ) / sliderItems[0].clientWidth)
    //         progress[0].style.width = `${current.innerHTML * percent}%`
    //     }
    // }

    slider.addEventListener('mousedown', (e) => {
        isDown = true
        slider.classList.add('active')
        startX = e.pageX - slider.offsetLeft
        scrollLeft = slider.scrollLeft
    })
    slider.addEventListener('mouseleave', () => {
        isDown = false
        slider.classList.remove('active')
    })
    slider.addEventListener('mouseup', () => {
        isDown = false
        slider.classList.remove('active')
       
    })
    slider.addEventListener('mousemove', (e) => {
        if(!isDown) return
        e.preventDefault()
        const x = e.pageX - slider.offsetLeft
        const walk = (x - startX) * 3  //scroll-fast
        // progressFunc(walk)
        slider.scrollLeft = scrollLeft - walk
    })

    const  scrollClick = index => {
        scrollSum = index * sliderItems[0].clientWidth
        percentSum = percent * (index + 1)
        helper(percentSum, false, index)
        scrollCarouselGen()
    }

    const helper = (percentSum, inc, clicked = false) => {
        if (parseInt(current[0].innerHTML) === 1) sliderItems[0].classList.add('aboutUsContentWaiterCarouselItemActive')
        for (let i = 0; i < sliderItems.length; i ++) {
            waiterInfoItems[i].style.display = 'none'
            sliderItems[i].classList.remove('aboutUsContentWaiterCarouselItemActive')
        }
        progress[0].style.width = `${percentSum}%`
        progress[1].style.width = `${percentSum}%`
        if (clicked) {
            current[0].innerHTML = parseInt(clicked) + 1
        } else {
            current[0].innerHTML = parseInt(current[0].innerHTML) + inc
        }
        current[1].innerHTML = current[0].innerHTML
        sliderItems[parseInt(current[0].innerHTML) - 1].classList.add('aboutUsContentWaiterCarouselItemActive')
        waiterInfoItems[parseInt(current[0].innerHTML) - 1].style.display = 'block'
    }

    const scrollCarousel = index => {
        if (scrollSum < 0) {
            scrollSum = 0
        }
        if (parseInt(current[0].innerHTML) === 1) sliderItems[0].classList.add('aboutUsContentWaiterCarouselItemActive')
        // if (parseInt(current[0].innerHTML) === sliderItems.length) sliderItems[sliderItems.length - 1].classList.add('aboutUsContentWaiterCarouselItemActive')
        if (scrollSum > carouselWidth ) scrollSum = carouselWidth
        if (index % 2 === 0) {
            scroll = - sliderItems[0].clientWidth
            if (scrollSum > 0) {
                percentSum -= percent
                helper(percentSum, -1)
            }
        } else { 
            scroll = sliderItems[0].clientWidth
            if (parseInt(current[0].innerHTML) < sliderItems.length) {
                percentSum += percent
                helper(percentSum, 1)
            }
        }
        scrollSum += scroll
        scrollCarouselGen()
    }
    const scrollCarouselGen = () => {
        slider.scrollTo({
            left: scrollSum,
            behavior: 'smooth',
            top: 0
        })
    }
}
start()
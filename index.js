let headerDropDown = document.getElementsByClassName('headDropDown')
let dropDownCont = document.getElementsByClassName('dropDownProfile')
let hidden = document.getElementById('hidden')
const videoPicture = document.getElementsByClassName('coursePageContentRightItem')
const navCont = document.getElementsByClassName('navCont')
const headButton = document.getElementsByClassName('headButton')[0]
const headButtonStickesT = document.getElementsByClassName('headButtonStickesT')[0]
const headButtonStickesC = document.getElementsByClassName('headButtonStickesC')[0]
const headButtonStickesB = document.getElementsByClassName('headButtonStickesB')[0]
const eyeShow = document.getElementById('eyeShow')
const inputPassword = document.getElementById('inputPassword')
let showToggle = true
let hamburgerToggle = true


const show = () => {
    if (showToggle) {
        inputPassword.type = 'text'
        showToggle = false
    } else {
        inputPassword.type = 'password'
        showToggle = true
    }
}

const dropDown = i => {
    dropDownCont[i].classList.add('show')
    hidden.classList.add('show')
    for (let j = 0; j < dropDownCont.length; j++) 
        if (i !== j)
            dropDownCont[j].classList.remove('show')
    if (i <= 2) {
        hamburgerToggle = false
        hamburger()
    }
}

const close = () => {
    for (let i = 0; i < headerDropDown.length; i++){
        dropDownCont[i].classList.remove('show')
    }
    hidden.classList.remove('show')
    navCont[0].classList.remove('show')
    hamburgerToggle = false
    hamburger()
}

const hamburger = () => {
    if (hamburgerToggle) {
        hamburgerToggle = false
        navCont[0].classList.add('show')
        hidden.classList.add('show')
        headButton.classList.add('headButtonAnime')
        headButtonStickesT.classList.add('headButtonAnimeStickesT')
        headButtonStickesC.classList.add('headButtonAnimeStickesC')
        headButtonStickesB.classList.add('headButtonAnimeStickesB')
    } else {
        hamburgerToggle = true
        navCont[0].classList.remove('show')
        hidden.classList.remove('show')
        headButton.classList.remove('headButtonAnime')
        headButtonStickesT.classList.remove('headButtonAnimeStickesT')
        headButtonStickesC.classList.remove('headButtonAnimeStickesC')
        headButtonStickesB.classList.remove('headButtonAnimeStickesB')
    }
}

const opacity = index => videoPicture[index].classList.add('opacity')

for (let i = 0; i < headerDropDown.length; i ++){
    headerDropDown[i].addEventListener('click', () => dropDown(i))
}

for (let i = 0; i < videoPicture.length; i ++) {
    videoPicture[i].addEventListener('click', () => opacity(i))       
}

eyeShow.addEventListener('click', show)

headButton.addEventListener('click', hamburger)

hidden.addEventListener('click', close)




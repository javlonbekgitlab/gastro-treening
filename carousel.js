const slider = document.querySelector('.vacancyPageContentBottomCarousel')
const sliderItems = document.getElementsByClassName('vacancyPageContentBottomCarouselItem')
const nav = document.getElementsByClassName('vacancyPageContentBottomCarouselNav')
const carouselWidth = sliderItems.length * sliderItems[0].clientWidth - sliderItems[0].clientWidth
let isDown = false
let startX
let scrollLeft
let scroll 
let scrollSum = 0 

for (let i = 0; i < nav.length; i++){
    nav[i].addEventListener('click', () => scrollCarousel(i))
}



slider.addEventListener('mousedown', (e) => {
    isDown = true
    slider.classList.add('active')
    startX = e.pageX - slider.offsetLeft
    scrollLeft = slider.scrollLeft
})
slider.addEventListener('mouseleave', () => {
    isDown = false
    slider.classList.remove('active')
})
slider.addEventListener('mouseup', () => {
    isDown = false
    slider.classList.remove('active')
})
slider.addEventListener('mousemove', (e) => {
    if(!isDown) return
    e.preventDefault()
    const x = e.pageX - slider.offsetLeft
    const walk = (x - startX) * 3 //scroll-fast
    slider.scrollLeft = scrollLeft - walk
})

const scrollCarousel = index => {
    if(scrollSum < 0)
        scrollSum = 0
    if (scrollSum > carouselWidth ) scrollSum = carouselWidth    
    index === 0 ? scroll = - sliderItems[0].clientWidth : scroll = sliderItems[0].clientWidth
    scrollSum += scroll
    slider.scrollTo({
        left: scrollSum,
        behavior: 'smooth',
        top: 0
    })
}
